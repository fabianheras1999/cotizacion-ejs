const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const app = express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({
    encoded: true
}));

//declarar una arrays de objectos
let datos = [{
        matricula: "2019030353",
        nombre: "Heras Rodriguez Fabian Ivan",
        sexo: 'M',
        materias: ["Ingles", "Base de datos", "Tecnologia I"]
    },
    {
        matricula: "2020030667",
        nombre: "Victor Donnovan Romero Sandoval",
        sexo: 'M',
        materias: ["Ingles", "Base de datos", "Tecnologia I"]
    },
    {
        matricula: "202003007",
        nombre: "Almogabar Vazquez Yarlen de Jesus",
        sexo: 'F',
        materias: ["Ingles", "Base de datos", "Tecnologia I"]
    }
]

app.get("/", (req, res) => {

    res.render('index', {
        titulo: "Lista de Alumnos",
        nombre: "Heras Rodriguez Fabian Ivan",
        grupo: "8-3",
        listado: datos
    })

});
app.get("/index", (req, res) => {

    res.render('index', {
        titulo: "Lista de Alumnos",
        nombre: "Heras Rodriguez Fabian Ivan",
        grupo: "8-3",
        listado: datos
    })

});

app.get('/tabla', (req, res) => {
    const params = {
        numero: req.query.numero
    }
    res.render('tabla', params);

})

app.post('/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    }
    res.render('tabla', params);
})

app.get('/cotizacion', (req, res) => {
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazo: req.query.plazo
    }
    res.render('cotizacion', params);

})

app.post('/cotizacion', (req, res) => {
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazo: req.body.plazo
    }
    res.render('cotizacion', params);
})


// la pagina del error va al final de los get/post
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/public/error.html')
})

const puerto = 3000;
app.listen(puerto, () => {

    console.log("Iniciando puerto");
});